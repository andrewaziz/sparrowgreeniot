#!/bin/bash
echo "Starting border router..."
myfile="/home/user/sparrow/products/sparrow-border-router/myactivesensors.txt"
if [ -f "$myfile" ] ; then
    rm -f "$myfile"
fi  
myserver = "$(cd /home/user/sparrow/products/sparrow-border-router && xterm -e make connect-full)"
until myserver; do
    if [ -f "$myfile" ] ; then
        rm -f "$myfile"
    fi  
    echo "Router has crashed. Restarting..." >&2
    myserver = "$(cd /home/user/sparrow/products/sparrow-border-router && xterm -e make connect-full)"
    sleep 4
done
